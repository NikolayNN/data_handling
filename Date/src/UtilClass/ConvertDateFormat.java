package UtilClass;


import Interface.ConvertDateFormatInterface;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


public class ConvertDateFormat implements ConvertDateFormatInterface {

    @Override
    public void convertDate(String date) {

        DateTimeFormatter stringFormatter = DateTimeFormatter.ofPattern("EEEE, MMM d, yyyy hh:mm:ss a", Locale.ENGLISH);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDateTime localDateTime = LocalDateTime.parse(date, stringFormatter);
        String stringDate = localDateTime.format(dateTimeFormatter);

        System.out.println("\nКонвертация строка-дата-строка (со сменой формата):");
        System.out.println(stringDate);

    }
}
