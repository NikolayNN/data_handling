package UtilClass;

import Interface.BirthdayInterface;

import java.time.LocalDateTime;
import static java.time.temporal.ChronoUnit.*;

public class Birthday implements BirthdayInterface {

    private LocalDateTime localDateTime = LocalDateTime.now();
    private LocalDateTime birthDay;

    public Birthday(LocalDateTime birthDay) {
        this.birthDay = birthDay;
    }
    public Birthday(int year, int month, int day, int hour, int minute, int second) {
        birthDay = LocalDateTime.of(year, month, day, hour, minute, second);
    }

    public LocalDateTime getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDateTime birthDay) {
        this.birthDay = birthDay;
    }

    @Override
    public void birthdayCount() {

        long years = YEARS.between(birthDay, localDateTime);
        long months = MONTHS.between(birthDay, localDateTime);
        long days = DAYS.between(birthDay, localDateTime);
        long hours = HOURS.between(birthDay, localDateTime);
        long minutes = MINUTES.between(birthDay, localDateTime);
        long seconds = SECONDS.between(birthDay, localDateTime);

        System.out.println("Разница между днем рождения и текущей датой в годах/месяцах/днях/часах/минутах/секундах");
        System.out.printf("years: %d month: %d days: %d hours: %d minutes: %d seconds: %d \t %n", years, months, days, hours, minutes, seconds);


    }
}
