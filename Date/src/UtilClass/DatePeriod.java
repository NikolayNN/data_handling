package UtilClass;

import Interface.DatePeriodInterface;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static java.lang.Math.abs;
import static java.time.temporal.ChronoUnit.DAYS;

public class DatePeriod implements DatePeriodInterface {
    String strDateOne;
    String strDateTwo;

    public DatePeriod(String strDateOne, String strDateTwo) {
        this.strDateOne = strDateOne;
        this.strDateTwo = strDateTwo;
    }

    public String getStrDateOne() {
        return strDateOne;
    }

    public void setStrDateOne(String strDateOne) {
        this.strDateOne = strDateOne;
    }

    public String getStrDateTwo() {
        return strDateTwo;
    }

    public void setStrDateTwo(String strDateTwo) {
        this.strDateTwo = strDateTwo;
    }

    @Override
    public void datePeriodCount() {


        try {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

            LocalDate dateOne = LocalDate.parse(strDateOne, dateTimeFormatter);
            LocalDate dateTwo = LocalDate.parse(strDateTwo, dateTimeFormatter);

            System.out.println("\nРазница между dateOne и dateTwo: " + abs(DAYS.between(dateOne, dateTwo)) + " дней.");

        } catch (DateTimeParseException e) {
            System.out.println("Date is not parsable!");
            System.out.println(e.getMessage());
        }


    }
}
