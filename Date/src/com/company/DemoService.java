package com.company;

import Interface.ConvertDateFormatInterface;
import Interface.DatePeriodInterface;
import Interface.DemoServiceIntarface;
import UtilClass.Birthday;
import UtilClass.ConvertDateFormat;
import UtilClass.DatePeriod;

public class DemoService implements DemoServiceIntarface {

    @Override
    public void demoService() {

        //разница между днем рождения и текущим временем
        Birthday birthday = new Birthday(1991, 04, 01, 19, 30, 25);
        birthday.birthdayCount();

        //разница между двумя разными датами в днях
        DatePeriodInterface datePeriod = new DatePeriod("23.10.1991", "23.10.1992");
        datePeriod.datePeriodCount();

        //конвертация строки в дату и обратно в строку другого формата
        ConvertDateFormatInterface convertDateFormat = new ConvertDateFormat();
        convertDateFormat.convertDate("Friday, Aug 12, 2016 12:10:56 PM");
    }
}
