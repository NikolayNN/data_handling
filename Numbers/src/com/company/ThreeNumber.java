package com.company;

import Interface.ThreeNumberInterface;

public class ThreeNumber implements ThreeNumberInterface {

    @Override
    public void sumNumber(String one, String two, String three) {

        Double firstNumber = Double.parseDouble(one);
        Double secondNumber = Double.parseDouble(two);
        Double thirdNumber = Double.parseDouble(three);

        if (firstNumber + secondNumber == thirdNumber) {
            System.out.println("\n" + three + " является суммой " + one + " и " + two);
        } else {
            System.out.println("\n" + three + " не является суммой " + one + " и " + two);
        }

    }
}
