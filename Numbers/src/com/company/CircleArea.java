package com.company;

import Interface.CircleAreaInterface;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class CircleArea implements CircleAreaInterface {

    @Override
    public void circleAreaCount(double radius) {

        BigDecimal area = BigDecimal.valueOf(Math.pow(radius, 2)*Math.PI );
        System.out.println(area.setScale(50, ROUND_HALF_UP));
    }
}
