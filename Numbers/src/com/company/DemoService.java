package com.company;

import Interface.CircleAreaInterface;
import Interface.DemoServiceInterface;
import Interface.MaxMinInterface;
import Interface.ThreeNumberInterface;

public class DemoService implements DemoServiceInterface {

    @Override
    public void demoService() {

        //вычисляем площадь круга
        CircleAreaInterface circleArea = new CircleArea();
        double radius = 2.1;
        circleArea.circleAreaCount(radius);

        //проверяем, является ли третье число суммой первого и второго
        ThreeNumberInterface threeNumber = new ThreeNumber();
        threeNumber.sumNumber("0.1", "0.15", "0.25");

        //поиск максимального и минимального числа
        MaxMinInterface maxMin = new MaxMin();
        maxMin.maxMinSearch(13.1, 34.23, 1.1);
    }
}
