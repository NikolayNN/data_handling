package com.company;


import Interface.MaxMinInterface;

import static java.lang.Math.*;

public class MaxMin implements MaxMinInterface {

    @Override
    public void maxMinSearch(double one, double two, double three) {

        double minNumber = min(min(one, two), three);
        double maxNumber = max(max(one, two), three);

        System.out.println("\n" + minNumber + " минимальное число, " + maxNumber + " максимальное число");
    }
}
